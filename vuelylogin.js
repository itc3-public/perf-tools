/**
 * @name vuelylogin
 *
 * @desc Logs into Vuely. Provide your username and password as environment variables when running the script, i.e:
 * `VUELY_USER=ashleypogue@itc3.io VUELY_PWD=Cricket2012 node vuelylogin.js`
 *
 */
const puppeteer = require('puppeteer')
const screenshot = 'vuely.png';
(async () => {
  const browser = await puppeteer.launch({headless: true})
  const page = await browser.newPage()
  await page.goto('https://vuely.itc3.io/#/session/login')
  await page.type('#login_field', process.env.VUELY_USER)
  await page.type('#password', process.env.VUELY_PWD)
  await page.click('[name="commit"]')
  await page.waitForNavigation()
  await page.screenshot({ path: screenshot })
  browser.close()
  console.log('See screenshot: ' + screenshot)
})()
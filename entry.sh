#!/bin/bash

set -xe

export CHROME_PATH=${CHROME_PATH:-/usr/bin/chromium-browser}
# export DISPLAY=${DISPLAY-:99}

sudo chown -Rv 1000:100 /tmp/chrome-data

exec supervisord &

export CHROME_PATH=/usr/bin/chromium-browser

sleep 15

node vuelylogin.js
# lighthouse ${FLAGS}
lighthouse --chrome-flags="--headless --no-sandbox" ${FLAGS}